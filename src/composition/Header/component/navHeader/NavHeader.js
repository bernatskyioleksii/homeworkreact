const NavHeader = () => {
	return ( 
		<nav className="menu">
                    <ul className="menu-list">
                        <li className="menu-list-item">Shop</li>
                        <li className="menu-list-item">Men</li>
                        <li className="menu-list-item">Women</li>
                        <li className="menu-list-item">Combos</li>
                        <li className="menu-list-item">Joggers</li>
                    </ul>
                </nav>
	 );
}
 
export default NavHeader;