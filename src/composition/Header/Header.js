import { Link } from "react-router-dom";
import { ReactComponent as Logo } from "./component/logo/logo.svg";
import PropTypes from "prop-types";

import Favorite from "./component/icons/Favorite";
import ShoppingBasket from "./component/icons/ShoppingBasket";
import NavHeader from "./component/navHeader/NavHeader";
import SearchHeader from "./component/searchHeader/SearchHeader";

import "./Header.scss";

const Header = ({ favorite, basket }) => {
  return (
    <header className="header">
      <div className="container">
        <div className="header__wrapper">
          <div className="header__logo">
            <Link to="/" className="logo">
              <Logo />
            </Link>
          </div>
          <NavHeader />
          <SearchHeader />
          <div className="header__actions">
            <div className="header__icon-list">
              <div className="icon-favorite">
                <span className="count">
                     <Favorite />
                      {favorite.length}
                </span>
              </div>
              <div className="icon-basket">
                     <ShoppingBasket />
                     {basket.length}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  favorite:PropTypes.array, 
  basket:PropTypes.array
}

export default Header;
