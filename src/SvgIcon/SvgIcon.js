
const SvgIcon = ({name}) => (
	<span className={`svg-icon svg-${name}`}>
		<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
            <use xlinkHref={`#img-${name}`}></use>
		</svg>
	</span>)


export const SvgIconBase = ({id,h,w}) => (
	<svg viewBox={`0 0 ${h} ${w}`} xmlns="http://www.w3.org/2000/svg">
		<use xlinkHref={`#${id}`}></use>
	</svg>)

export const SvgIconSpan = ({name, children}) => (
	<span className={`svg-icon svg-${name}`}>
		{children}
	</span>)

export const SvgIconBox = ({name, h,w}) => (
	<SvgIconSpan className={`svg-icon svg-${name}`}>
		<SvgIconBase id={name} h={h} w={w} />
	</SvgIconSpan>)

export default SvgIcon