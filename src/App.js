import { useState, useEffect } from "react";

import Header from "./composition/Header/Header";
import Footer from "./composition/Footer/Footer";
import ClothingStore from "./components/ClothingStore/ClothingStore";

function App() {
  //const [favorite, setFavorite] = useState([])      чому 1 варіант не працює?
  const [favorite, setFavorite] = useState(
    JSON.parse(localStorage.getItem("favorite")) || []
  );
  const [basket, setBasket] = useState(
    JSON.parse(localStorage.getItem("basket")) || []
  );

  // 1 варіант
  // useEffect(() => {
  // 	const favorite = ;
  // 	if (favorite) {console.log(favorite);
  // 		setFavorite(favorite);
  // 	}
  //}, []);

  //localStorage.clear()

  useEffect(() => {
    localStorage.setItem("favorite", JSON.stringify(favorite));
  }, [favorite]);
  useEffect(() => {
    localStorage.setItem("basket", JSON.stringify(basket));
  }, [basket]);

  const hendleFavorites = (item) => {
    const isAdded = favorite.some((favorite) => favorite.id === item.id);
    if (isAdded) {
      const updatedFavorites = favorite.filter(
        (favorite) => favorite.id !== item.id
      );
      setFavorite(updatedFavorites);
    } else {
      setFavorite([...favorite, item]);
    }
  };

  const handleBasket = (item) => {
    const isAdded = basket.some((basket) => basket.id === item.id);
    if (isAdded) {
      return;
    }
    setBasket([...basket, item]);
  };

  return (
    <div className="g-app">
      <Header favorite={favorite} basket={basket} />
      <h1 className="home-work__title">Home Work 2</h1>
      <ClothingStore
        hendleFavorites={hendleFavorites}
        handleBasket={handleBasket}
        favorite={favorite}
        basket={basket}
      />
      <Footer />
    </div>
  );
}

export default App;
