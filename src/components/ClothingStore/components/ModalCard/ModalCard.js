import PropTypes from "prop-types";
import ModalWrapper from "../../../Modal/ModalWrapper";
import Modal from "../../../Modal/Modal";
import ModalHeader from "../../../Modal/ModalHeader";
import ModalBody from "../../../Modal/ModalBody";
import ModalFooter from "../../../Modal/ModalFooter";

const ModalCard = ({
  title,
  desc,
  handleClose,
  isOpen,
  price,
  article,
  img,
  hendleOk,
}) => {
  const handleOutside = (event) => {
    if (!event.target.closest(".modal")) {
      handleClose();
    }
  };

  return (
    <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
      <Modal>
        <ModalHeader>
          <div className="image">
            <img src={img} alt={title} />
          </div>
        </ModalHeader>
        <ModalBody>
          <h3>{title}</h3>
          <p>
            <i>{desc}</i>
          </p>
          <p>${price}</p>
          <p>article{article}</p>
        </ModalBody>
        <ModalFooter
          textFirst="Add to cart"
          textSecondary="Close"
          clickFirst={hendleOk}
          clickSecondary={handleClose}
        />
      </Modal>
    </ModalWrapper>
  );
};
ModalCard.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  hendleFavorites: PropTypes.func,
  handleClose: PropTypes.func,
  isOpen: PropTypes.bool,
  img: PropTypes.string,
  alt: PropTypes.string,
};

export default ModalCard;
