import PropTypes from "prop-types";

import ClothingStoreCard from "../ClothingStoreCard/ClothingStoreCard";

const ClothingStoreMan = ({
  menProducts,
  handleModal,
  handleCurrentPost,
  hendleFavorites,
  favorite,
}) => {
  const clothingStoreItems = menProducts.map((item, id) => (
    <ClothingStoreCard
      isFavorite={favorite.some((favorite) => favorite.id === item.id)}
      product={item}
      key={id}
      handleModal={handleModal}
      handleCurrentPost={handleCurrentPost}
      hendleFavorites={hendleFavorites}
    />
  ));

  return <div className="clothing__content">{clothingStoreItems}</div>;
};
ClothingStoreMan.propTypes = {
  menProducts: PropTypes.array,
  handleModal: PropTypes.func,
  handleCurrentPost: PropTypes.func,
  hendleFavorites: PropTypes.func,
  favorite: PropTypes.array,
};

export default ClothingStoreMan;
