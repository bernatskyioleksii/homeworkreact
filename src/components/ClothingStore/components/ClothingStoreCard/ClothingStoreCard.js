import PropTypes from "prop-types";
import cx from "classnames";

import StarIcon from "../../../../Icon/StarIcon";
import Arrow from "../../../../Icon/Arrow";
import Button from "../../../Button/Button";

const ClothingStoreCard = ({
  product,
  handleModal,
  handleCurrentPost,
  hendleFavorites,
  isFavorite,
}) => {
  const { name, productImage, productText } = product;

  const newColor = isFavorite ? "red" : "white";

  return (
    <div className={cx("clothing__card", { "card--selected": false })}>
      <div className="card__img">
        <img src={productImage} alt={name} />
        <StarIcon
          fill={newColor}
          onClick={() => {
            hendleFavorites(product, newColor);
          }}
        />
      </div>
      <h3 className="card__title">{name}</h3>
      <p className="card__subtitle">
        {productText}
        <Arrow
          className="arrow"
          onClick={() => {
            handleModal();
            handleCurrentPost(product);
          }}
        />
      </p>
      <Button
        classNames={"card_box"}
        onClick={() => {
          handleModal();
          handleCurrentPost(product);
        }}
      >
        {"Add to cart"}
      </Button>
    </div>
  );
};

ClothingStoreCard.propTypes = {
  product: PropTypes.object,
  handleModal: PropTypes.func,
  handleCurrentPost: PropTypes.func,
  hendleFavorites: PropTypes.func,
};

export default ClothingStoreCard;
