import PropTypes from "prop-types";

import ClothingStoreCard from "../ClothingStoreCard/ClothingStoreCard";

const ClosingStoreWomen = ({
  womenProducts,
  handleModal,
  handleCurrentPost,
  hendleFavorites,
  favorite,
}) => {
  const clothingStoreItems = womenProducts.map((item, id) => (
    <ClothingStoreCard
      isFavorite={favorite.some((favorite) => favorite.id === item.id)}
      product={item}
      key={id}
      handleModal={handleModal}
      handleCurrentPost={handleCurrentPost}
      hendleFavorites={hendleFavorites}
    />
  ));

  return <div className="clothing__content">{clothingStoreItems}</div>;
};
ClosingStoreWomen.propTypes = {
  womenProducts: PropTypes.array,
  handleModal: PropTypes.func,
  handleCurrentPost: PropTypes.func,
  hendleFavorites: PropTypes.func,
  favorite: PropTypes.array,
};

export default ClosingStoreWomen;
