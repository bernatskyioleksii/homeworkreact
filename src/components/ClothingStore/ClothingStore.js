import { useEffect, useState } from "react";
import { sendRequest } from "../../helpers/sendRequest";
import PropTypes from "prop-types";

import ClothingStoreMan from "./components/ClothingStoreMan/ClothingStoreMan";
import ClothingStoreWomen from "./components/ClothingStoreWomen/ClothingStoreWomen";
import ModalCard from "./components/ModalCard/ModalCard";

import "./ClothingStore.scss";

const ClothingStore = ({ hendleFavorites, handleBasket, favorite }) => {
  const [clothingMan, setClothingMan] = useState([]);
  const [clothingWomen, setClothingWomen] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [currentPost, setCurrentPost] = useState({});
  const handleModal = () => setIsOpen(!isOpen);
  const handleCurrentPost = (cardPost) => setCurrentPost(cardPost);

  useEffect(() => {
    sendRequest("/data.json").then(({ product }) => {
      const menProducts = product.filter((item) => item.category === "man");
      const womenProducts = product.filter((item) => item.category === "woman");
      setClothingMan(menProducts);
      setClothingWomen(womenProducts);
    });
  }, []);

  return (
    <div className="container">
      <div className="clothing__container">
        <div className="clothing__title">Categories For Men</div>
        <ClothingStoreMan
          favorite={favorite}
          menProducts={clothingMan}
          handleModal={handleModal}
          handleCurrentPost={handleCurrentPost}
          hendleFavorites={hendleFavorites}
          handleBasket={handleBasket}
        />

        <div className="clothing__title">Categories For Women</div>
        <ClothingStoreWomen
          favorite={favorite}
          womenProducts={clothingWomen}
          handleModal={handleModal}
          handleCurrentPost={handleCurrentPost}
          hendleFavorites={hendleFavorites}
          handleBasket={handleBasket}
        />
      </div>
      <ModalCard
        title={currentPost.name}
        img={currentPost.productImage}
        desc={currentPost.productText}
        price={currentPost.price}
        article={currentPost.article}
        isOpen={isOpen}
        handleClose={handleModal}
        hendleOk={() => {
          handleBasket(currentPost);
          handleModal();
        }}
      />
    </div>
  );
};

ClothingStore.propTypes = {
  hendleFavorites: PropTypes.func,
  handleBasket: PropTypes.func,
  favorite: PropTypes.array,
};

export default ClothingStore;
