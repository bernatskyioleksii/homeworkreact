import PropTypes from "prop-types";

import "./Modal.scss";

const ModalWrapper = ({ children, isOpen, handleOutside }) => {
  return (
    <>
      {isOpen && (
        <div className="modal-wrapper" onClick={handleOutside}>
          {children}
        </div>
      )}
    </>
  );
};

ModalWrapper.defaultProps = {
  isOpen: false,
};

ModalWrapper.propTypes = {
  children: PropTypes.any,
  isOpen: PropTypes.bool,
  handleOutside: PropTypes.func,
};

export default ModalWrapper;
