import cx from "classnames";
import PropTypes from "prop-types";

const Modal = ({ children, classNames }) => {
  
  const isActive = true || (true && false && (true || false));

  return (
    <div className={cx("modal", classNames, { active: isActive })}>
      <div className={cx("modal-box", classNames)}>{children}</div>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.any,
  classNames: PropTypes.string,
};

export default Modal;
