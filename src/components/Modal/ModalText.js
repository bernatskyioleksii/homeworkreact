import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
//import ModalClose from "./ModalClose";

const ModalText = ({ title, desc, handleOk, handleClose, isOpen, hendleFavorites }) => {
  const handleOutside = (event) => {
    if (!event.target.closest(".modal")) {
      handleClose();
    }
  };

  return (
    <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
      <Modal>
        {/* <ModalClose click={handleClose} /> */}
        <ModalBody>
          <div>{title}</div>
          <p>{desc}</p>
        </ModalBody>
        {/* <ModalFooter textFirst="Add to cart" textSecondary="Close" clickFirst={hendleFavorites}/> */}
        <ModalFooter textFirst="Add to cart" clickFirst={hendleFavorites} />
      </Modal>
    </ModalWrapper>
  );
};

export default ModalText;
