import PropTypes from "prop-types";
import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalHeader from "./ModalHeader";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";

import "./ModalImage.scss";

const ModalImage = ({ title, desc, handleOk, handleClose, isOpen }) => {
  const handleOutside = (event) => {
    if (!event.target.closest(".modal")) {
      handleClose();
    }
  };
  return (
    <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
      <Modal>
        <ModalClose click={handleClose} />
        <ModalHeader>
          <figure>
            <img
              src="https://media.md-fashion.com.ua/images/60/80/c98449f910be2e5033b2729b9db9.jpg"
              alt="pictur"
            />
          </figure>
        </ModalHeader>
        <ModalBody>
          <div>{title}</div>
          <p>{desc}</p>
        </ModalBody>
        <ModalFooter
          textFirst="Add to cart"
          textSecondary="Close"
          clickFirst={handleOk}
          clickSecondary={handleClose}
        />
      </Modal>
    </ModalWrapper>
  );
};

ModalImage.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  handleOk: PropTypes.func,
  handleClose: PropTypes.func,
  isOpen: PropTypes.bool,
};

export default ModalImage;
