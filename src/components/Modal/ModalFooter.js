import PropTypes from "prop-types";
import Button from "../Button/Button";

const ModalFooter = ({
  textFirst,
  textSecondary,
  clickFirst,
  clickSecondary,
}) => {
  return (
    <div className="button-wrapper">
      {textFirst && (
        <Button boxView onClick={clickFirst}>
          {textFirst}
        </Button>
      )}
      {textSecondary && (
        <Button underlineView onClick={clickSecondary}>
          {textSecondary}
        </Button>
      )}
    </div>
  );
};

ModalFooter.propTypes = {
  textFirst: PropTypes.string,
  textSecondary: PropTypes.string,
  clickFirst: PropTypes.func,
  clickSecondary: PropTypes.func,
};

export default ModalFooter;
